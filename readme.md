Benötigt:
* Wordpress 6.x
* Plugins:
  - Commons Booking (aktuell Version 2.7.3)
 * Eigene Plugins:
   - Frieda & Friedrich Custom User Fields
   - (optional) frieda_feedback_plugin
   - (optional) frieda_init_plugin (in diesem Repository)
* Eigenes Theme:
  - friedatheme2


--------------
- Wordpress installieren (Adminuser und passwort festlegen)
- Per FTP einloggen ...
  * unter /wp-content/plugins das Verzeichnis frieda_custom_user_fields mit Inhalt anlegen
  * unter /wp-content/plugins das Verzeichnis frieda_feedback_plugin mit Inhalt anlegen
  * unter /wp-content/plugins das Verzeichnis frieda_init_plugin mit Inhalt anlegen
  * unter /wp-content/themens das Verzeichnis friedatheme (Repository: friedatheme2) mit Inhalt anlegen und ggf. andere Themens löschen
- Als Admin im Wordpressbackend einloggen ...
  * alle Aktualisierungen von WP-Core, WP-Plugins, Übersetzungen installeren
unter Plugins "WP-CFM" installieren und aktivieren
  * unter Plugins "Commons Booking" installieren und aktivieren
  * unter Plugins "Frieda & Friedrich Custom User Fields" aktivieren
  * Nicht benötigte Plugins entfernen: WP Mail SMTP
  * unter Themes "friedatheme2" aktivieren

- Unter Einstellungen->Allgemein
  * ...Titel der Website: Freies Lastenrad Zittau
  * ...Untertitel: Freie Lastenräder für Zittau
  * ...Mitgliedschaft: "Jeder kann sich registrieren" auswählen

- Unter Einstellungen->Diskussion:
  * ...Avatare anzeigen ausschalten

- Unter Einstellungen->Medien:
  * "Meine Uploads in monats- und jahresbasierten Ordnern organisieren" abwählen

- Unter Einstellungen->Permalinks:
  * ...Permalink-struktur auf "Beitragsname" setzen "https://example.de/beispielbeitrag/"

* In den CB-Einstellungen eine neue "Karte" anlegen:
  - Titel z.B. "Dresdenansicht"
  - Kartendarstellung: Höhe 450px
  - Zoom: Initialie Zoomstufe 12
  - Die Geokoordinaten unter Kartenpositionierung (mitte) angeben: 51.069, 13.7348
  - Unter Medien das gewollte Marker-Symbol (PNG z.B. 42x62px) hochladen und unter "Benutzerdefinierter Marker" auswählen, Zielgröße (z.B. 25x37) und Ankerpunkt (z.B. 12.5x37) angeben.
   - TODO: Individuelle Symbole ermöglichen.


Nach Erstellen der Karte das Plugin "frieda_init_plugin" aktivieren, um die Seiten
  - Startseite
  - Meine Buchungen

zu generieren/überschreiben. Nach der Aktivierung kann das Plugin deaktiviert und ggf. gelöscht werden.

In den CB-Einstellungen, unter Allgemein als Buchungsseite die neue Seite "Meine Buchungen" auswählen.

* Artikel Anlegen:
  - Titel eingeben
  - Artikelfoto auswählen
  - Beschreibungstext eintippen (keine Fotos im Text)


* Theme anpassen:
  - Unter Design->Customzier->Websiteninformationen: Logo angeben (185x185px oder besser 190x190px) und Favicon/Website-Icon wählen



----------------
TODO:
- Zeigt Commonsbooking2 keine Buchungkommentare als Popup-Tooltip im Buchungskalender öffentlich? So ist es.
- Feiertage
- Müssen Feier-, Urlaubs- und Schließtage je Artikel einzeln pro Zeitintervall eingetragen werden? (Benötig wird Ersatz für Friedablock sowie einfachere eintragung von feiertagen)
- Karte beibringen, schöne F&F-Symbole je nach Lastenradtyp anzuzeigen
- Feedback system: Datenbankspeicherung wieder implementieren. Feedback-Daten an cb_booking-CPT Meta anhängen: update_post_meta($booking_id, 'feedback_comment', $comment);
- Friedastat neu implementieren
- Disable WP and PHP debug mode
- Delete User
- Logout / normales ausloggen, über /wp-login.php?action=logout&redirect_to=https%3A%2F%2Ffriedafriedrich.de&_wpnonce=fdba6157ee


- "Du kannst freiwillig andere Frieda & Friedrich-Fans inspirieren, indem du den geplanten Nutzungszweck im Buchungskalender als Kommentar öffentlich teilst. Beispiele: Transport von Zimmerpflanzen, Kindergeburtstag, Umzug. Nur der eingegebene Text (bitte keine persönliche Angaben) wird veröffentlicht."


