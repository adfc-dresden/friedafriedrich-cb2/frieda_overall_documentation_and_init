<?php
/**
 * Plugin Name: Frieda & Friedrich Initialization plugin
 * Description: Creates/overwrites pages and settings on activation
 */



function on_plugin_activation() {
    /**
     * Create page "Meine Buchungen"
     */
    create_page("Meine Buchungen", file_get_contents(plugin_dir_path(__FILE__) . "page-meine-buchungen.html"));

    /**
     * Create page "Artikel" (die Übersicht)
     */
    create_page("Artikel", file_get_contents(plugin_dir_path(__FILE__) . "page-artikel.html"));

    /**
     * Create homepage "Startseite"
     * Automatically find the active Commonsbooking2 map_id and replace the placeholder-id
     */  
    $frontpage_content = file_get_contents(plugin_dir_path(__FILE__) . "front-page.html");    
    $cb_map_posts = get_posts(array('post_type' => 'cb_map', 'post_status' => 'publish'));
    if (count($cb_map_posts) > 0) {
        $cb_map_id = $cb_map_posts[0]->ID;
        $frontpage_content = str_replace("[cb_map id=123]", "[cb_map id=$cb_map_id]", $frontpage_content);
    }   
    create_page("Startseite", $frontpage_content);
    
    update_options();
    
   
    

}
register_activation_hook( __FILE__, 'on_plugin_activation' );



function create_page($title, $content) {
    
    $searchpost = array(
        'title'    => $title,
        'post_type'     => 'page',
        'post_status'     => 'publish'
    );

    $existing_posts = get_posts($searchpost);
    
    if (count($existing_posts) > 1) {
        die("Multiple pages with title exists already (update which?): $title");
        return false;
    } else if (count($existing_posts) == 1) {
        $page = $existing_posts[0];
        $page->post_content = $content;
        return wp_update_post( $page );
    } else {
        $page = array(
        'post_title'    => $title,
        'post_type'     => 'page',
        'post_status'     => 'publish',
        'post_content' => $content
        );
        return wp_insert_post( $page );
    }

}



function update_options() {

    $commonsbooking_options_general = get_option('commonsbooking_options_general');
    $commonsbooking_options_templates = get_option('commonsbooking_options_templates');
    $commonsbooking_options_restrictions = get_option('commonsbooking_options_restrictions');
    $commonsbooking_options_reminder = get_option('commonsbooking_options_reminder');
    $commonsbooking_options_export = get_option('commonsbooking_options_export');
    $commonsbooking_options_bookingcodes = get_option('commonsbooking_options_bookingcodes');
    $commonsbooking_options_api = get_option('commonsbooking_options_api');

    if (!$commonsbooking_options_api) {
        // this option is not create by default by cb2 on first activation
        $commonsbooking_options_api = array();
    }
    
    if (!is_array($commonsbooking_options_general) or
        !is_array($commonsbooking_options_templates) or
        !is_array($commonsbooking_options_restrictions) or
        !is_array($commonsbooking_options_reminder) or
        !is_array($commonsbooking_options_export) or
        !is_array($commonsbooking_options_bookingcodes) or
        !is_array($commonsbooking_options_api) ) {
        die("cb2 options not found");
    }
    
    $commonsbooking_options_general['booking-comment-active'] = 'on'; // 'on' or false
    $commonsbooking_options_general['booking-comment-title'] = 'Buchungskommentar';
    $commonsbooking_options_general['booking-comment-description'] = 'Hier kann ein Kommentar zur Buchung eingegeben werden.';
    
    $wpurl = get_bloginfo('wpurl');
    $absender_name = html_entity_decode(get_bloginfo('name'), encoding: "UTF-8") . "-Buchungssystem";
    $absender_email = "lastenrad@adfc-zittau.de";
    $grussformel = '<h3>Hallo {{user:first_name}},</h3>';
    $schlussformel = <<<EOF
    <p>Danke und viele Grüße,<br>
    freies Lastenrad Zittau - ein Projekt des ADFC Zittau</p>
    <p><small>Der ADFC Zittau ist eine Ortsgruppe des ADFC Sachsen e.V., Bautzner Straße 25, 01099 Dresden - <a href="https://www.adfc-sachsen.de/impressum">IMPRESSUM</a></small></p>
    EOF;    
    
    $commonsbooking_options_templates['emailheaders_from-email'] = $absender_email;
    $commonsbooking_options_templates['emailheaders_from-name'] = $absender_name;
    $commonsbooking_options_templates['emailtemplates_mail-booking-confirmed-subject'] = 'Deine Buchung von {{item:post_title}} {{booking:formattedBookingDate}}';
    
    // TODO (Tel. nur für dringende Angelegenheiten: 0351-21295009) 
    $commonsbooking_options_templates['emailtemplates_mail-booking-confirmed-body'] = <<<EOF
    $grussformel
    <p>vielen Dank für deine Buchung von {{item:post_title}} {{booking:formattedBookingDate}}.</p>
    Abholung: <strong>{{booking:pickupDatetime}}</strong><br>
    Rückgabe: <strong>{{booking:returnDatetime}}</strong>
    {{location:formattedPickupInstructions}}
    {{booking:formattedBookingCode}}
    <h4>Standort</h4>
    {{location:formattedAddress}}
    {{location:formattedContactInfoOneLine}}
    <br>
    <strong>Klicke hier, um deine Buchung zu sehen und zu stornieren: {{booking:bookingLink}}</strong><br>
    <br>
    <strong>Hinweis:</strong> Du musst eingeloggt sein, um deine Buchung sehen zu können.<br>
    Wenn dich der Link zur Hauptseite führt, logge dich zunächst ein und klicke dann erneut auf den Link.
    
    <h4>Deine Daten</h4>
    <p><em>Achte bitte darauf, dass deine Angaben zu Name und Adresse mit denen auf deinem Personalausweis übereinstimmen. Sonst wird dir das Lastenrad nicht ausgehändigt.</em></p>
    Name: {{user:first_name}} {{user:last_name}}<br>
    Anschrift: {{user:address}}<br>
    Telefon: {{user:phone}}
    
    
    <h4>Hinweise zum Fahren der Lastenräder</h4>
    <p>Bitte geh mit den Lastenfahrrädern sorgsam um. Schließe sie ordentlich an, wenn du sie nicht benutzt.<br>
    <p>Du kannst in die Transportkiste alles packen, was hinein passt und die maximale Zuladung nicht überschreitet.<br>
    <p>Kinder nur transportieren, wenn das Lastenrad laut Beschreibung dafür geeignet ist und grundsätzlich nur gut angeschnallt.<br>
    <p>Bitte sei vorsichtig mit großen Gegenständen, die oben rausgucken – beim Fahren musst du immer einen freien Blick haben.<br>
    <p>Bedenke immer, dass ein Lastenrad bei voller Beladung einen etwas längeren Bremsweg haben kann. Fahre also nicht zu schnell...<br>
    <p>Bei Schäden oder Problemen kannst du dich direkt an uns wenden. Bringe das Lastenrad bitte unbedingt zu der Station zurück, an der du sie ausgeliehen hast und informiere die Station schnellstmöglich über den Schaden.<br>
    <p>Bitte erzähle uns von deinen Erlebnissen mit unseren Lastenfahrrädern und schicke uns ein schönes Foto, das wir für die Homepage oder Facebook verwenden können. Wir freuen uns auf deinen Bericht!</p>

    $schlussformel
    EOF;
    
    $commonsbooking_options_templates['emailtemplates_mail-booking-canceled-subject'] = 'Buchung storniert: {{item:post_title}} {{booking:formattedBookingDate}}';
 
    $commonsbooking_options_templates['emailtemplates_mail-booking-canceled-body'] = <<<EOF
    $grussformel
    deine Buchung von {{item:post_title}} am Standort {{location:post_title}} {{booking:formattedBookingDate}} wurde storniert.        
    $schlussformel
    EOF;
    
    $commonsbooking_options_templates['booking-confirmed-notice'] = 'Deine Buchung wurde bestätigt. Du hast eine Bestätigungs-E-Mail erhalten.';
    $commonsbooking_options_templates['item-not-available'] = 'Dieser Artikel ist derzeit nicht buchbar.';
    $commonsbooking_options_templates['location-without-items'] = 'Keine Artikel an diesem Standort verfügbar.';
    $commonsbooking_options_templates['show_contactinfo_unconfirmed'] = 'on'; // 'on' or false
    $commonsbooking_options_templates['text_hidden-contactinfo'] = 'Bitte bestätige die Buchung, um die Kontaktdaten für die Abholung und Rückgabe zu sehen.';
    $commonsbooking_options_templates['text_book-this-item'] = 'Buche den Artikel an dieser Station';
    $commonsbooking_options_templates['label-booking-button'] = 'Artikel buchen';
    $commonsbooking_options_templates['image_listing_crop'] = false; // 'on' or false
    $commonsbooking_options_templates['image_listing_small_width'] = '60';
    $commonsbooking_options_templates['image_listing_small_height'] = '60';
    $commonsbooking_options_templates['image_listing_medium_width'] = '100';
    $commonsbooking_options_templates['image_listing_medium_height'] = '100';
    $commonsbooking_options_templates['colorscheme_primarycolor'] = '#84AE53';
    $commonsbooking_options_templates['colorscheme_secondarycolor'] = '#506CA9';
    $commonsbooking_options_templates['colorscheme_buttoncolor'] = '#74ce3c';
    $commonsbooking_options_templates['colorscheme_acceptcolor'] = '#74ce3c';
    $commonsbooking_options_templates['colorscheme_cancelcolor'] = '#d5425c';
    $commonsbooking_options_templates['colorscheme_holidaycolor'] = '#ff9218';
    $commonsbooking_options_templates['colorscheme_greyedoutcolor'] = '#c4c4c4';
    $commonsbooking_options_templates['colorscheme_backgroundcolor'] = '#f6f6f6';
    $commonsbooking_options_templates['colorscheme_noticebackgroundcolor'] = '#FFF9C5'; 
 

    $commonsbooking_options_restrictions['restrictions-from-email'] = $absender_email;
    $commonsbooking_options_restrictions['restrictions-from-name'] = $absender_name;
    $commonsbooking_options_restrictions['restrictions-repair-subject'] = 'Ausfall von {{item:post_title}} für deine Buchung {{booking:formattedBookingDate}}';

    $commonsbooking_options_restrictions['restrictions-repair-body'] = <<<EOF
    $grussformel
    <p>Leider ist der von dir gebuchte Artikel {{item:post_title}} von {{restriction:formattedStartDateTime}} bis voraussichtlich {{restriction:formattedEndDateTime}} nicht mehr verwendbar. <strong>Dies betrifft deine Buchung {{booking:formattedBookingDate}}</strong>.</p>
    <p>Der Grund ist: {{restriction:hint}} </p>

    <p>Wir mussten deine Buchung für diesen Zeitraum stornieren. Du erhältst eine Bestätigung der Stornierung in einer separaten E-Mail.
    Bitte buche den Artikel erneut für einen anderen Zeitraum oder überprüfe auf unserer Website, ob ein alternativer Artikel verfügbar ist.</p>
    $schlussformel
    EOF;
    
    $commonsbooking_options_restrictions['restrictions-hint-subject'] = 'Nutzungsbeschränkung für {{item:post_title}} für deine Buchung {{booking:formattedBookingDate}}';
    
    $commonsbooking_options_restrictions['restrictions-hint-body'] = <<<EOF
    $grussformel
    <p>Der von dir gebuchte Artikel {{item:post_title}} kann von {{restriction:formattedStartDateTime}} bis voraussichtlich {{restriction:formattedEndDateTime}} nur eingeschränkt genutzt werden.</p>
    <p>Der Grund ist: {{restriction:hint}}</p>
    <p><strong>Dies betrifft deine Buchung {{booking:formattedBookingDate}}</strong></p>

    <p>Bitte prüfe, ob du deine Buchung trotz der Einschränkungen beibehalten möchtest. <br>
    Wenn nicht, storniere bitte deine Buchung über den folgenden Link:</p>
    <p>{{booking:BookingLink}}</p>
    <p>Wir bemühen uns, die Einschränkung zu schnell wie möglich zu beheben. Wenn wir die Einschränkung vorher beheben können, erhältst du eine weitere Mail.</p>
    $schlussformel
    EOF;
    
    $commonsbooking_options_restrictions['restrictions-restriction-cancelled-subject'] = 'Einschränkung für Artikel {{item:post_title}} ist aufgehoben';
    $commonsbooking_options_restrictions['restrictions-restriction-cancelled-body'] = <<<EOF
    $grussformel
    <p>Der Artikel {{item:post_title}} ist nun wieder voll nutzbar.</p>
    <p>Dies betrifft unter anderem deine Buchung {{booking:formattedBookingDate}}.</p>
    <p>Hier der Link zu deiner Buchung: {{booking:BookingLink}}</p> 
    $schlussformel
    EOF;
    
    $commonsbooking_options_reminder['pre-booking-reminder-activate'] = 'on'; // 'on' or false
    
    $commonsbooking_options_reminder['pre-booking-reminder-subject'] = 'Bevorstehende Buchung von {{item:post_title}} {{booking:formattedBookingDate}}';
    $commonsbooking_options_reminder['pre-booking-reminder-body'] = <<<EOF
    <h3>Hallo {{user:first_name}},</h3>
    <p>dein Buchungszeitraum für den Artikel {{item:post_title}} beginnt in Kürze.<br>
    <br>
    Dein Buchungszeitraum: {{booking:formattedBookingDate}}<br><br>

    Wenn du den von dir gebuchten Artikel nicht mehr benötigst, storniere bitte die Buchung, damit andere Personen ihn möglicherweise nutzen können.<br>
    Für Buchungdetails und Stornierungsmöglichkeiten, klicke auf den Link zu deiner Buchung: {{booking:bookingLink}}</p>
    $schlussformel
    EOF;
    
    $commonsbooking_options_reminder['pre-booking-days-before'] = 2;
    $commonsbooking_options_reminder['pre-booking-time'] = 'on'; // 'on' or false
    
    $commonsbooking_options_reminder['post-booking-notice-activate'] = 'on'; // 'on' or false
    $commonsbooking_options_reminder['post-booking-notice-subject'] = 'War alles in Ordnung mit dem Lastenrad?';
  
    $commonsbooking_options_reminder['post-booking-notice-body'] = <<<EOF
    <p>Liebe Nutzerin, lieber Nutzer des Lastenrades <i>{{item:post_title}}</i>:</p>
          <p>Der ADFC Zittau bemühen sich, den Lastenrad-Verleih reibungslos zu gestalten. Daher sind wir auf präzise Rückmeldungen angewiesen. Bitte teile uns Mängel oder Probleme mit.</p>
          <p><i>Auch wenn du schon in der Verleihstation oder auf dem Ausleihzettel Mängel oder Probleme angegeben hast, erreichen diese die Lastenrad-Anbieter meist nicht schnell genug. Daher ist die Rückmeldung auf diesem Wege sehr wichtig.</i></p>
          <p>Bitte auf den zutreffenden Link klicken:</p>
          <table width="100%">
             <tr>
                <td>
                   <table style="border-spacing: 10px;">
                      <tr>
                         <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=ok" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Alles in Ordnung!</a></td>
                         <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=nouse" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Verleih fand nicht statt</a></td>
                         <td align="center" style="border-radius: 3px;background-color: #e9703e"><a href="FEEDBACKENDPOINT?hash={{booking#post_name}}&amp;feedback=attention" target="_blank" style="font-size: 16px;font-family: Helvetica, Arial, sans-serif;color: #ffffff;text-decoration: none;text-decoration: none;border-radius: 3px;padding: 12px 18px;border: 1px solid #e9703e;display: inline-block">Es gab Probleme</a></td>
                      </tr>
                   </table>
                </td>
             </tr>
          </table>

    <p>DONATION_REQUEST({{item:ID}})</p>
    $schlussformel
    EOF;
    
    $commonsbooking_options_bookingcodes['bookingcodes'] = 'Annaberg-Buchholz,Aue-Bad Schlema,Auerbach,Bautzen,Budyšin,Bischofswerda,Borna,Brand-Erbisdorf,Chemnitz,Coswig,Crimmitschau,Delitzsch,Dippoldiswalde,Dresden,Döbeln,Eilenburg,Flöha,Freiberg,Freital,Geithain,Glauchau,Görlitz,Grimma,Großenhain,Hainichen,Hohenstein-Ernstthal,Hoyerswerda,Leipzig,Wojerecy,Kamenz,Kamjenc,Klingenthal,Limbach-Oberfrohna,Löbau,Marienberg,Markkleeberg,Meißen,Mittweida,Niesky,Oelsnitz,Oschatz,Pirna,Plauen,Radeberg,Radebeul,Reichenbach,Riesa,Rochlitz,Schkeuditz,Schwarzenberg,Sebnitz,Stollberg,Torgau,Weißwasser,Běła Woda,Werdau,Wurzen,Zittau,Zschopau,Zwickau';
    
    $commonsbooking_options_api['api-activated'] = 'on';
    $commonsbooking_options_api['apikey_not_required'] = 'on';

    update_option('commonsbooking_options_general', $commonsbooking_options_general);
    update_option('commonsbooking_options_templates', $commonsbooking_options_templates);
    update_option('commonsbooking_options_restrictions', $commonsbooking_options_restrictions);
    update_option('commonsbooking_options_reminder', $commonsbooking_options_reminder);
    update_option('commonsbooking_options_export', $commonsbooking_options_export);
    update_option('commonsbooking_options_bookingcodes', $commonsbooking_options_bookingcodes);
    update_option('commonsbooking_options_api', $commonsbooking_options_api);

    /*************************************************************/
    
    $new_user_notification_email_from = "$absender_name <$absender_email>";
    
    $new_user_notification_email_body = <<<EOF
    <h3>Hallo {{user:first_name}}, danke für deine Registrierung!</h3>

    <p>Klicke auf den Link, um dein Passwort festzulegen und damit deine Registrierung zu vervollständigen:</p>

    <p><a href="{{pwreseturl}}">{{pwreseturl}}</a></p>

    <h4>Bestätigung der Nutzungsbedingungen und der Datenschutzerklärung</h4>

    <p>Link zu Nutzungsbedingungen: <a href="$wpurl/Nutzungsbedingungen.pdf">$wpurl/Nutzungsbedingungen.pdf</a><br>
    Link zu Datenschutzerklärung: <a href="$wpurl/Datenschutzerklaerung.pdf">$wpurl/Datenschutzerklaerung.pdf</a>
    </p>

    $schlussformel

    EOF;

    // TODO: 
    // <p>Im Anhang findest du die aktuellen Nutzungsbedingungen und unsere Datenschutzerklärung. Solltest du dein Einverstädnis zurückziehen und/oder dein Nutzerkonto löschen wollen, dann ist das auf der Webseite unter Konto // -> Profil oder mit einer formlosen Nachricht an uns möglich.</p>

    $new_user_notification_email_subject = "Deine Registrierung - Freies Lastenrad Zittau";
    
    update_option('new_user_notification_email_from', $new_user_notification_email_from);
    update_option('new_user_notification_email_subject', $new_user_notification_email_subject);
    update_option('new_user_notification_email_body', $new_user_notification_email_body);

}


?>